import { RestCalendar } from '@asap-hub/squidex';

export type SquidexEntityEvent =
  | 'Created'
  | 'Published'
  | 'Updated'
  | 'Unpublished'
  | 'Deleted';

export type CalendarEvent = `Calendars${SquidexEntityEvent}`;
export type ExternalAuthorEvent = `ExternalAuthors${SquidexEntityEvent}`;
export type LabEvent = `Labs${SquidexEntityEvent}`;
export type TeamEvent = `Teams${SquidexEntityEvent}`;
export type UserEvent = `Users${SquidexEntityEvent}`;
export type ResearchOutputEvent = `ResearchOutputs${SquidexEntityEvent}`;

export type EventBusEvent =
  | CalendarEvent
  | ExternalAuthorEvent
  | LabEvent
  | TeamEvent
  | UserEvent
  | ResearchOutputEvent;

export type LabPayload = {
  type: LabEvent;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
  };
};

export type ExternalAuthorPayload = {
  type: ExternalAuthorEvent;
  timestamp: string;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
    created: string;
    lastModified: string;
    version: number;
    data: { [x: string]: { iv: unknown } | null };
  };
};

export type ResearchOutputPayload = {
  type: ResearchOutputEvent;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
  };
};

export type TeamPayload = {
  type: TeamEvent;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
    data: {
      outputs: { iv: string[] };
    };
    dataOld?: {
      outputs: { iv: string[] };
    };
  };
};

export type UserPayload = {
  type: UserEvent;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
    created: string;
    lastModified: string;
    version: number;
    data: { [x: string]: { iv: unknown } | null };
    dataOld?: { [x: string]: { iv: unknown } | null };
  };
};

type CalendarPayloadData = Pick<
  RestCalendar['data'],
  'googleCalendarId' | 'resourceId' | 'name' | 'color'
>;
export type CalendarPayload = {
  type: CalendarEvent;
  payload: {
    $type: 'EnrichedContentEvent';
    type: SquidexEntityEvent;
    id: string;
    data: CalendarPayloadData;
    dataOld?: CalendarPayloadData;
    version: number;
  };
};
